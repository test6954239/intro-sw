import logo from './logo.svg';
import './App.css';
import imagen1 from '../src/assets/angel.png'
import imagen2 from '../src/assets/KAYAB (1).png'
import imagen3 from '../src/assets/KAYAB (2).png'
import imagen4 from '../src/assets/KAYAB.png'
import imagen5 from '../src/assets/revolucion.png'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <div>
        <img src={imagen1} alt='imagen1'/>
        <img src={imagen2}  alt='imagen2'/>
        <img src={imagen3}  alt='imagen3'/>
        <img src={imagen4}  alt='imagen4'/>
        <img src={imagen5}  alt='imagen5'/>
      </div>
    </div>
  );
}

export default App;
